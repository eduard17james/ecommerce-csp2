// Packages and Dependencies
 const express = require("express");
 const mongoose = require('mongoose');
 const dotenv = require("dotenv");
 

//  Server Setup
  const app = express();
  dotenv.config();
  app.use(express.json());
  const secret = process.env.CONNECTION_STRING;
  const port = process.env.PORT;

// Application Routes


// Database connect
  mongoose.connect(secret)
  let connectStatus = mongoose.connection;
  connectStatus.on('open', () => console.log('Database is Connected'));

// Gateway Response
  app.get('/', (req, res) => {
    res.send(`Welcome to App`);
  });
  app.listen(port, () => console.log(`Server is on port ${port}`));
