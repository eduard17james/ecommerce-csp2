// [Section]Dependencies and Modules
   const mongoose = require("mongoose");
 
// [Section] Schema / Document Blueprint
   const orderSchema = new mongoose.Schema({
   	totalAmount: {
         type: Number,
         required: [true, 'Product Name is Required']
   	},
   	purchasedOn: {
         type: Date,
         default: new Date()
   	},
      description: [
         {
            userId: {
               type: String,
               required: [true, "User's Id is required"]
            },
            productId: {
               type: String,
               required: [true, "Product's Id is required"]
            }
         }
      ]
   })


// [Section] Model
   const Order = mongoose.model("Order", orderSchema);
   module.exports = Order;