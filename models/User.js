// dependencies and modules
  const mongoose = require('mongoose');



// blueprint schema
   const userSchema = new mongoose.Schema({
     email: {
     	type: String,
     	required: [true, 'Email is Required']
     }, 
     password: {
     	type: String,
     	required: [true, 'Password is Required']
     }, 
     isAdmin: {
     	type: Boolean,
     	default: false
     },  
  });


// model
   const User = mongoose.model('User', userSchema)
   module.exports = User;